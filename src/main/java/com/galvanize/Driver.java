package com.galvanize;

public interface Driver {

    String quoteColumn(String column);

    String quoteTable(String table);

    String quoteValue(String value);

}
